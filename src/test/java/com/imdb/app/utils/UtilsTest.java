package com.imdb.app.utils;

import com.imdb.app.models.Principals;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.imdb.app.utils.Utils.getPrincipals;
import static com.imdb.app.utils.Utils.parsePrincipalsTSV;
import static org.junit.jupiter.api.Assertions.*;

/**
 * This is a test for Utils
 */
class UtilsTest {

    private static final String PRINCIPALS = "tt0000003\t2\tnm1770680\tproducer\tproducer\t\\N";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * This method is a test for tsv parser for Principals
     */
    @Test
    void parsePrincipalsTSVTest() {
        List<Principals> principals = parsePrincipalsTSV();
        assertNotNull(principals);
        assertEquals(principals.size(), 13);
        assertEquals(principals.get(0).getTconst(), "tt0000001");
        assertEquals(principals.get(1).getCategory(), "director");
        assertNotEquals(principals.get(2).getNconst(), "nm0721526");
    }

    /**
     * This method is a test for converting tsv String to Principals entity
     */
    @Test
    void getPrincipalsTest() {
        Principals principals = getPrincipals(PRINCIPALS);
        assertNotNull(principals);
        assertEquals(principals.getTconst(), "tt0000003");
        assertEquals(principals.getCategory(), "producer");
        assertNotEquals(principals.getNconst(), "nm0721526");
    }
}