package com.imdb.app.controllers;

import com.imdb.app.models.NameBasics;
import com.imdb.app.models.TitleBasics;
import com.imdb.app.services.NameBasicsService;
import com.imdb.app.services.TitleBasicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This is imdb client controller
 */
@RestController
@CrossOrigin
@RequestMapping(path = "/v1/imdb_client")
public class IMDBController {

    @Autowired
    private NameBasicsService nameBasicsService;

    @Autowired
    private TitleBasicsService titleBasicsService;

    /**
     * This API is for getting a list of actor/actress by the genre of their movies
     *
     * @param genres is the genre of the movie
     * @return is a list of actor/actress personal information
     */
    @GetMapping("/actor_by_genre/{genres}")
    @ResponseBody
    public List<NameBasics> findByGenre(@PathVariable(required = false) String genres) {
        return nameBasicsService.findByGenre(genres);
    }

    /**
     * This API is for getting a list of actor/actress information , consist of name , the genre ,
     * and the count of their genre , by nconst (identifier of actor/actress)
     *
     * @param nconst is identifier of actor/actress
     * @return is a list of actor/actress nconst,name,genre,genre count
     */
    @GetMapping("/genre_count_by_actor/{nconst}")
    @ResponseBody
    public List<Object[]> findGenreCountByNconst(@PathVariable(required = false) String nconst) {
        return nameBasicsService.findGenreCountByNconst(nconst);
    }

    /**
     * This API is for getting a list of titles , coincidence of two actor/actress
     *
     * @param name1 is name of first actor/actress
     * @param name2 is name of second actor/actress
     * @return is a list of title information
     */
    @GetMapping("/actor_by_coincidence/{name1}/{name2}")
    @ResponseBody
    public List<TitleBasics> findByCoincidence(@PathVariable(required = true) String name1,
                                               @PathVariable(required = true) String name2) {
        return titleBasicsService.findByCoincidence(name1, name2);
    }

}
