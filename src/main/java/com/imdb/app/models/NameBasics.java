package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Set;

/**
 * This is NameBasics entity
 * <p>
 * Contains the following information for names:
 * nconst (string) - alphanumeric unique identifier of the name/person
 * primaryName (string)– name by which the person is most often credited
 * birthYear – in YYYY format
 * deathYear – in YYYY format if applicable, else '\N'
 * primaryProfession (array of strings)– the top-3 professions of the person
 * knownForTitles (array of tconsts) – titles the person is known for
 */
@Entity
@Table(name = "name_basics", indexes = @Index(columnList = "primaryName"))
public class NameBasics {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nconst;
    private String primaryName;
    private String birthYear;
    private String deathYear;
    @ElementCollection
    private Set<String> primaryProfession;
    @ElementCollection
    private Set<String> knownForTitles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNconst() {
        return nconst;
    }

    public void setNconst(String nconst) {
        this.nconst = nconst;
    }

    public String getPrimaryName() {
        return primaryName;
    }

    public void setPrimaryName(String primaryName) {
        this.primaryName = primaryName;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getDeathYear() {
        return deathYear;
    }

    public void setDeathYear(String deathYear) {
        this.deathYear = deathYear;
    }

    public Set<String> getPrimaryProfession() {
        return primaryProfession;
    }

    public void setPrimaryProfession(Set<String> primaryProfession) {
        this.primaryProfession = primaryProfession;
    }

    public Set<String> getKnownForTitles() {
        return knownForTitles;
    }

    public void setKnownForTitles(Set<String> knownForTitles) {
        this.knownForTitles = knownForTitles;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof NameBasics))
            return false;
        NameBasics other = (NameBasics) o;
        return this.nconst.equals(other.nconst) &&
                this.primaryName.equals(other.primaryName) &&
                this.birthYear.equals(other.birthYear) &&
                this.deathYear.equals(other.deathYear);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (nconst != null) {
            result = 31 * result + nconst.hashCode();
        }
        if (primaryName != null) {
            result = 31 * result + primaryName.hashCode();
        }
        return result;
    }

}
