package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;

/**
 * This is Principals entity
 * <p>
 * Contains the principal cast/crew for titles
 * tconst (string) - alphanumeric unique identifier of the title
 * ordering (integer) – a number to uniquely identify rows for a given titleId
 * nconst (string) - alphanumeric unique identifier of the name/person
 * category (string) - the category of job that person was in
 * job (string) - the specific job title if applicable, else '\N'
 * characters (string) - the name of the character played if applicable, else '\N'
 */
@Entity
@Table(name = "principals", indexes = @Index(columnList = "ordering,nconst,category"))
public class Principals {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tconst;
    private Integer ordering;
    private String nconst;
    private String category;
    private String job;
    private String characters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTconst() {
        return tconst;
    }

    public void setTconst(String tconst) {
        this.tconst = tconst;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getNconst() {
        return nconst;
    }

    public void setNconst(String nconst) {
        this.nconst = nconst;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Principals))
            return false;
        Principals other = (Principals) o;
        return this.tconst.equals(other.tconst) &&
                this.ordering.equals(other.ordering) &&
                this.category.equals(other.category) &&
                this.nconst.equals(other.nconst);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (tconst != null) {
            result = 31 * result + tconst.hashCode();
        }
        if (category != null) {
            result = 31 * result + category.hashCode();
        }
        return result;
    }

}
