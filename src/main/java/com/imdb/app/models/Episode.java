package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;

/**
 * This is Episode entity
 * <p>
 * Contains the tv episode information. Fields include:
 * tconst (string) - alphanumeric identifier of episode
 * parentTconst (string) - alphanumeric identifier of the parent TV Series
 * seasonNumber (integer) – season number the episode belongs to
 * episodeNumber (integer) – episode number of the tconst in the TV series
 */
@Entity
@Table(name = "episode", indexes = @Index(columnList = "parentTconst"))
public class Episode {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tconst;
    private String parentTconst;
    private Integer seasonNumber;
    private Integer episodeNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTconst() {
        return tconst;
    }

    public void setTconst(String tconst) {
        this.tconst = tconst;
    }

    public String getParentTconst() {
        return parentTconst;
    }

    public void setParentTconst(String parentTconst) {
        this.parentTconst = parentTconst;
    }

    public Integer getSeasonNumber() {
        return seasonNumber;
    }

    public void setSeasonNumber(Integer seasonNumber) {
        this.seasonNumber = seasonNumber;
    }

    public Integer getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(Integer episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Episode))
            return false;
        Episode other = (Episode) o;
        return this.tconst.equals(other.tconst) &&
                this.parentTconst.equals(other.parentTconst) &&
                this.seasonNumber.equals(other.seasonNumber) &&
                this.episodeNumber.equals(other.episodeNumber);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (tconst != null) {
            result = 31 * result + tconst.hashCode();
        }
        if (parentTconst != null) {
            result = 31 * result + parentTconst.hashCode();
        }
        return result;
    }

}
