package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Set;

/**
 * This is Akas entity
 * <p>
 * Contains the following information for titles:
 * titleId (string) - a tconst, an alphanumeric unique identifier of the title
 * ordering (integer) – a number to uniquely identify rows for a given titleId
 * title (string) – the localized title
 * region (string) - the region for this version of the title
 * language (string) - the language of the title
 * types (array) - Enumerated set of attributes for this alternative title. One or more of the following: "alternative", "dvd", "festival", "tv", "video", "working", "original", "imdbDisplay". New values may be added in the future without warning
 * attributes (array) - Additional terms to describe this alternative title, not enumerated
 * isOriginalTitle (boolean) – 0: not original title; 1: original title
 */
@Entity
@Table(name = "akas", indexes = @Index(columnList = "ordering,title"))
public class Akas {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String titleId;
    private Integer ordering;
    private String title;
    private String region;
    private String language;
    @ElementCollection
    private Set<String> types;
    @ElementCollection
    private Set<String> attributes;
    private Boolean isOriginalTitle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Set<String> getTypes() {
        return types;
    }

    public void setTypes(Set<String> types) {
        this.types = types;
    }

    public Set<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Set<String> attributes) {
        this.attributes = attributes;
    }

    public Boolean getOriginalTitle() {
        return isOriginalTitle;
    }

    public void setOriginalTitle(Boolean originalTitle) {
        isOriginalTitle = originalTitle;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Akas))
            return false;
        Akas other = (Akas) o;
        return this.titleId.equals(other.titleId) &&
                this.ordering.equals(other.ordering) &&
                this.title.equals(other.title);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (titleId != null) {
            result = 31 * result + titleId.hashCode();
        }
        if (ordering != null) {
            result = 31 * result + ordering.hashCode();
        }
        if (title != null) {
            result = 31 * result + title.hashCode();
        }
        return result;
    }

}
