package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;

/**
 * This is Ratings entity
 * <p>
 * Contains the IMDb rating and votes information for titles
 * tconst (string) - alphanumeric unique identifier of the title
 * averageRating – weighted average of all the individual user ratings
 * numVotes - number of votes the title has received
 */
@Entity
@Table(name = "ratings")
public class Ratings {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tconst;
    private Double averageRating;
    private Long numVotes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTconst() {
        return tconst;
    }

    public void setTconst(String tconst) {
        this.tconst = tconst;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Long getNumVotes() {
        return numVotes;
    }

    public void setNumVotes(Long numVotes) {
        this.numVotes = numVotes;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Ratings))
            return false;
        Ratings other = (Ratings) o;
        return this.tconst.equals(other.tconst) &&
                this.numVotes.equals(other.numVotes);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (tconst != null) {
            result = 31 * result + tconst.hashCode();
        }
        if (numVotes != null) {
            result = 31 * result + numVotes.hashCode();
        }
        return result;
    }

}
