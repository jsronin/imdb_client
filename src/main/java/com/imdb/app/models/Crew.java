package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Set;

/**
 * This is Crew entity
 * <p>
 * Contains the director and writer information for all the titles in IMDb. Fields include:
 * tconst (string) - alphanumeric unique identifier of the title
 * directors (array of nconsts) - director(s) of the given title
 * writers (array of nconsts) – writer(s) of the given title
 */
@Entity
@Table(name = "crew")
public class Crew {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tconst;
    @ElementCollection
    private Set<String> directors;
    @ElementCollection
    private Set<String> writers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTconst() {
        return tconst;
    }

    public void setTconst(String tconst) {
        this.tconst = tconst;
    }

    public Set<String> getDirectors() {
        return directors;
    }

    public void setDirectors(Set<String> directors) {
        this.directors = directors;
    }

    public Set<String> getWriters() {
        return writers;
    }

    public void setWriters(Set<String> writers) {
        this.writers = writers;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Crew))
            return false;
        Crew other = (Crew) o;
        return this.tconst.equals(other.tconst);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (tconst != null) {
            result = 31 * result + tconst.hashCode();
        }
        return result;
    }

}
