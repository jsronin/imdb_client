package com.imdb.app.models;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Set;

/**
 * This is TitleBasics entity
 * <p>
 * Contains the following information for titles:
 * tconst (string) - alphanumeric unique identifier of the title
 * titleType (string) – the type/format of the title (e.g. movie, short, tvseries, tvepisode, video, etc)
 * primaryTitle (string) – the more popular title / the title used by the filmmakers on promotional materials at the point of release
 * originalTitle (string) - original title, in the original language
 * isAdult (boolean) - 0: non-adult title; 1: adult title
 * startYear (YYYY) – represents the release year of a title. In the case of TV Series, it is the series start year
 * endYear (YYYY) – TV Series end year. ‘\N’ for all other title types
 * runtimeMinutes – primary runtime of the title, in minutes
 * genres (string array) – includes up to three genres associated with the title
 */
@Entity
@Table(name = "title_basics", indexes = @Index(columnList = "titleType,primaryTitle"))
public class TitleBasics {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tconst;
    private String titleType;
    private String primaryTitle;
    private String originalTitle;
    private Boolean isAdult;
    private String startYear;
    private String endYear;
    private Long runtimeMinutes;
    @ElementCollection
    private Set<String> genres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTconst() {
        return tconst;
    }

    public void setTconst(String tconst) {
        this.tconst = tconst;
    }

    public String getTitleType() {
        return titleType;
    }

    public void setTitleType(String titleType) {
        this.titleType = titleType;
    }

    public String getPrimaryTitle() {
        return primaryTitle;
    }

    public void setPrimaryTitle(String primaryTitle) {
        this.primaryTitle = primaryTitle;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public Boolean getAdult() {
        return isAdult;
    }

    public void setAdult(Boolean adult) {
        isAdult = adult;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public Long getRuntimeMinutes() {
        return runtimeMinutes;
    }

    public void setRuntimeMinutes(Long runtimeMinutes) {
        this.runtimeMinutes = runtimeMinutes;
    }

    public Set<String> getGenres() {
        return genres;
    }

    public void setGenres(Set<String> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof TitleBasics))
            return false;
        TitleBasics other = (TitleBasics) o;
        return this.tconst.equals(other.tconst) &&
                this.titleType.equals(other.titleType) &&
                this.primaryTitle.equals(other.primaryTitle) &&
                this.originalTitle.equals(other.originalTitle);
    }

    @Override
    public final int hashCode() {
        int result = 17;
        if (tconst != null) {
            result = 31 * result + tconst.hashCode();
        }
        if (titleType != null) {
            result = 31 * result + titleType.hashCode();
        }
        if (primaryTitle != null) {
            result = 31 * result + primaryTitle.hashCode();
        }
        return result;
    }

}
