package com.imdb.app.services;

import com.imdb.app.repositories.EpisodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is an implementation of EpisodeService
 */
@Service
public class EpisodeServiceImpl implements EpisodeService {

    @Autowired
    EpisodeRepository episodeRepository;

    public EpisodeServiceImpl() {
    }

    public EpisodeServiceImpl(EpisodeRepository episodeRepository) {
        this.episodeRepository = episodeRepository;
    }
}
