package com.imdb.app.services;

import com.imdb.app.models.TitleBasics;
import com.imdb.app.repositories.TitleBasicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is an implementation of TitleBasicsService
 */
@Service
public class TitleBasicsServiceImpl implements TitleBasicsService {

    @Autowired
    TitleBasicsRepository titleBasicsRepository;

    public TitleBasicsServiceImpl() {
    }

    public TitleBasicsServiceImpl(TitleBasicsRepository titleBasicsRepository) {
        this.titleBasicsRepository = titleBasicsRepository;
    }

    /**
     * This API is for getting a list of titles , coincidence of two actor/actress
     *
     * @param name1 is name of first actor/actress
     * @param name2 is name of second actor/actress
     * @return is a list of title information
     */
    public List<TitleBasics> findByCoincidence(String name1, String name2) {
        return titleBasicsRepository.findByCoincidence(name1, name2);
    }

}
