package com.imdb.app.services;

import com.imdb.app.models.TitleBasics;

import java.util.List;

public interface TitleBasicsService {

    List<TitleBasics> findByCoincidence(String name1, String name2);

}
