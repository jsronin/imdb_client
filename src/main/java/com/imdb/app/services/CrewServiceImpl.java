package com.imdb.app.services;

import com.imdb.app.repositories.CrewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is an implementation of CrewService
 */
@Service
public class CrewServiceImpl implements CrewService {

    @Autowired
    CrewRepository crewRepository;

    public CrewServiceImpl() {
    }

    public CrewServiceImpl(CrewRepository crewRepository) {
        this.crewRepository = crewRepository;
    }


}
