package com.imdb.app.services;

import com.imdb.app.models.NameBasics;

import java.util.List;

public interface NameBasicsService {

    List<NameBasics> findByGenre(String genres);

    List<Object[]> findGenreCountByNconst(String nconst);

}
