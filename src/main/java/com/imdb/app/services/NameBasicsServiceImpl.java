package com.imdb.app.services;

import com.imdb.app.models.NameBasics;
import com.imdb.app.repositories.NameBasicsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is an implementation of NameBasicsService
 */
@Service
public class NameBasicsServiceImpl implements NameBasicsService {

    @Autowired
    NameBasicsRepository nameBasicsRepository;

    public NameBasicsServiceImpl() {
    }

    public NameBasicsServiceImpl(NameBasicsRepository nameBasicsRepository) {
        this.nameBasicsRepository = nameBasicsRepository;
    }

    /**
     * This method is for getting a list of actor/actress by the genre of their movies
     *
     * @param genres is the genre of the movie
     * @return is a list of actor/actress personal information
     */
    public List<NameBasics> findByGenre(String genres) {
        return nameBasicsRepository.findByGenre(genres);
    }

    /**
     * This API is for getting a list of actor/actress information , consist of name , the genre ,
     * and the count of their genre , by nconst (identifier of actor/actress)
     *
     * @param nconst is identifier of actor/actress
     * @return is a list of actor/actress nconst,name,genre,genre count
     */
    public List<Object[]> findGenreCountByNconst(String nconst) {
        return nameBasicsRepository.findGenreCountByNconst(nconst);
    }

}
