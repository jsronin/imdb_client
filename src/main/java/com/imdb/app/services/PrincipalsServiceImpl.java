package com.imdb.app.services;

import com.imdb.app.repositories.PrincipalsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is an implementation of PrincipalsService
 */
@Service
public class PrincipalsServiceImpl implements PrincipalsService {

    @Autowired
    PrincipalsRepository principalsRepository;

    public PrincipalsServiceImpl() {
    }

    public PrincipalsServiceImpl(PrincipalsRepository principalsRepository) {
        this.principalsRepository = principalsRepository;
    }
}
