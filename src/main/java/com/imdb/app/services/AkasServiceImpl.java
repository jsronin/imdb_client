package com.imdb.app.services;

import com.imdb.app.repositories.AkasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is an implementation of AkasService
 */
@Service
public class AkasServiceImpl implements AkasService {

    @Autowired
    AkasRepository akasRepository;

    public AkasServiceImpl() {
    }

    public AkasServiceImpl(AkasRepository akasRepository) {
        this.akasRepository = akasRepository;
    }

}
