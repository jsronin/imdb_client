package com.imdb.app.services;

import com.imdb.app.repositories.RatingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This is an implementation of RatingsService
 */
@Service
public class RatingsServiceImpl implements RatingsService {

    @Autowired
    RatingsRepository ratingsRepository;

    public RatingsServiceImpl() {
    }

    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

}
