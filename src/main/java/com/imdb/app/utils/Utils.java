package com.imdb.app.utils;

import com.imdb.app.models.Principals;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * This is Utils class for parsing tsv files
 */
public class Utils {

    public static List<String> fileNameList = Arrays.asList("/title.principals.tsv");

    /**
     * This method is tsv parser for Principals
     *
     * @return is a list of Principals entity
     */
    public static List<Principals> parsePrincipalsTSV() {
        List<Principals> result = new ArrayList<>();
        try {
            Stream<String> principals = new BufferedReader(new InputStreamReader(new FileInputStream(Utils.class.getResource(fileNameList.get(0)).getFile()))).lines();
            principals.forEach(p -> {
                Principals item = getPrincipals(p);
                if (item != null) {
                    result.add(item);
                }
            });
        } catch (Exception e) {
        }
        return result;
    }

    /**
     * This method is for converting tsv String to Principals entity
     *
     * @param item is tsv String
     * @return is Principals entity
     */
    public static Principals getPrincipals(String item) {
        Principals result = new Principals();
        if (item == null || item.length() <= 0 || item.contains("tconst")) {
            return null;
        }
        String[] itemList = item.split("\t");
        if (itemList == null || itemList.length < 6) {
            return null;
        }
        result.setTconst(itemList[0].equals("\\N") ? "" : itemList[0]);
        result.setOrdering(itemList[1].equals("\\N") ? null : Integer.valueOf(itemList[1]));
        result.setNconst(itemList[2].equals("\\N") ? "" : itemList[2]);
        result.setCategory(itemList[3].equals("\\N") ? "" : itemList[3]);
        result.setJob(itemList[4].equals("\\N") ? "" : itemList[4]);
        result.setCharacters(itemList[5].equals("\\N") ? "" : itemList[5]);
        return result;
    }
}
