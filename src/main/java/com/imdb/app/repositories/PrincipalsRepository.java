package com.imdb.app.repositories;

import com.imdb.app.models.Principals;
import org.springframework.data.repository.CrudRepository;

public interface PrincipalsRepository extends CrudRepository<Principals, Integer> {

}
