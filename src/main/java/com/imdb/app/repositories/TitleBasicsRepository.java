package com.imdb.app.repositories;

import com.imdb.app.models.TitleBasics;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TitleBasicsRepository extends CrudRepository<TitleBasics, Integer> {

    String findByCoincidenceQuery = " select tb.* " +
            " from " +
            "  principals p " +
            "  join name_basics nb on nb.nconst = p.nconst " +
            "  join name_basics nb2 on nb2.nconst = p.nconst " +
            "  join title_basics tb on tb.tconst = p.tconst " +
            " where p.category = 'actor' and nb.primary_name = :name1 and nb2.primary_name = :name2  ";

    /**
     * This API is for getting a list of titles , coincidence of two actor/actress
     *
     * @param name1 is name of first actor/actress
     * @param name2 is name of second actor/actress
     * @return is a list of title information
     */
    @Query(value = findByCoincidenceQuery, nativeQuery = true)
    List<TitleBasics> findByCoincidence(@Param("name1") String name1, @Param("name2") String name2);

}
