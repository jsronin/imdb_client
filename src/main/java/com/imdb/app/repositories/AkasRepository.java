package com.imdb.app.repositories;

import com.imdb.app.models.Akas;
import org.springframework.data.repository.CrudRepository;

public interface AkasRepository extends CrudRepository<Akas, Integer> {

}
