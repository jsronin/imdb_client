package com.imdb.app.repositories;

import com.imdb.app.models.Crew;
import org.springframework.data.repository.CrudRepository;

public interface CrewRepository extends CrudRepository<Crew, Integer> {

}
