package com.imdb.app.repositories;

import com.imdb.app.models.Episode;
import org.springframework.data.repository.CrudRepository;

public interface EpisodeRepository extends CrudRepository<Episode, Integer> {

}
