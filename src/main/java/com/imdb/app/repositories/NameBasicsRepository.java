package com.imdb.app.repositories;

import com.imdb.app.models.NameBasics;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NameBasicsRepository extends CrudRepository<NameBasics, Integer> {

    String findByGenreQuery = " select nb.* " +
            " from " +
            "  title_basics tb " +
            "  join title_basics_genres tbg on tbg.title_basics_id = tb.id " +
            "  join principals p on p.tconst = tb.tconst " +
            "  join name_basics nb on nb.nconst = p.nconst " +
            " where p.category = 'actor' and (tbg.genres = :genres or :genres is null) ";

    String findGenreCountByNconst = " select " +
            "  p.nconst, " +
            "  nb.primary_name, " +
            "  tbg.genres, " +
            "  count(*) " +
            " from " +
            "  title_basics tb " +
            "  join title_basics_genres tbg on tbg.title_basics_id = tb.id " +
            "  join principals p on p.tconst = tb.tconst " +
            "  join name_basics nb on nb.nconst = p.nconst " +
            " where p.category = 'actor' and (p.nconst = :nconst or :nconst is null) " +
            " group by p.nconst, nb.primary_name, tbg.genres";

    /**
     * This method is for getting a list of actor/actress by the genre of their movies
     *
     * @param genres is the genre of the movie
     * @return is a list of actor/actress personal information
     */
    @Query(value = findByGenreQuery, nativeQuery = true)
    List<NameBasics> findByGenre(@Param("genres") String genres);

    /**
     * This API is for getting a list of actor/actress information , consist of name , the genre ,
     * and the count of their genre , by nconst (identifier of actor/actress)
     *
     * @param nconst is identifier of actor/actress
     * @return is a list of actor/actress nconst,name,genre,genre count
     */
    @Query(value = findGenreCountByNconst, nativeQuery = true)
    List<Object[]> findGenreCountByNconst(@Param("nconst") String nconst);

}
