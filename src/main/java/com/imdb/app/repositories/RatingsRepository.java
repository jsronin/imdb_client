package com.imdb.app.repositories;

import com.imdb.app.models.Ratings;
import org.springframework.data.repository.CrudRepository;

public interface RatingsRepository extends CrudRepository<Ratings, Integer> {

}
