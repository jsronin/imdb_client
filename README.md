IMDB Client Application
---

Welcome to the IMDB Client !

### Introduction

This is a simple application , based on IMDB Data sets , Using Spring Boot and PostgreSQL as DB 
* https://www.imdb.com/interfaces/
### The functionality
* Getting a list of actor/actress by the genre of their movies
* Getting a list of actor/actress information , consist of name , the genre ,and the count of their genre , by nconst (identifier of actor/actress)
* Getting a list of titles , coincidence of two actor/actress

##How to build and run
* mvn clean install
* mvn spring-boot:run
###For Docker deployment
* docker build imdb_cleint
* docker run -p 8000:8000 imdb_cleint
###API Documentation
* http://localhost:8000/swagger-ui.html
###Default Host and Port
* localhost:8000
* localhost:8000/v1/imdb_client/actor_by_genre/action
